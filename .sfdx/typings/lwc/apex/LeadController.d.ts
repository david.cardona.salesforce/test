declare module "@salesforce/apex/LeadController.createLead" {
  export default function createLead(param: {objLead: any}): Promise<any>;
}
