trigger Oppty_Trigger on Opportunity (before insert, after update) {
    if(trigger.isBefore && trigger.isInsert){
        Oppty_Handler.primeraOportunidadCuenta(trigger.new);
    }
    if(trigger.isAfter && trigger.isUpdate){
        List<Opportunity> lstOpptysNotificacion = new List<Opportunity>();
        for(Opportunity objOpptyNew:trigger.new){
            Opportunity opptyOld = trigger.oldMap.get(objOpptyNew.Id);
            if(objOpptyNew.StageName != opptyOld.StageName && (objOpptyNew.StageName == 'Closed Won' || objOpptyNew.StageName == 'Closed Lost')){
                lstOpptysNotificacion.add(objOpptyNew);
            }
        }
        if(!lstOpptysNotificacion.isEmpty()){
			Oppty_Handler.notificar(lstOpptysNotificacion);
        }
    }
}