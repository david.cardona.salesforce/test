import { LightningElement, track } from 'lwc';

import createLead from '@salesforce/apex/LeadController.createLead';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class NewLead extends LightningElement {
    @track isLoading = false;

    finish(result){
        let successMsg = "Prospect "+result+" successfully!";
        this.showToast("Success",successMsg);
        this.isLoading = false;
        const inputFields = [...this.template.querySelectorAll('lightning-input-field')];
        inputFields.forEach((field) => field.reset());
    }
    
    handleSubmit(event) {
        this.isLoading = true;
        event.preventDefault();

        createLead({
            objLead: event.detail.fields
        })
        .then(result => {
            console.log('***result:'+result);
            this.finish(result);
        })
        .catch(error => {
            console.log('***Error save lead:'+error);
        })
    }

    showToast(state,msg){
        this.dispatchEvent(
            new ShowToastEvent({
                title: state,
                message: msg,
                variant: state,
                mode: 'pester'
            }),
        );
    }
}