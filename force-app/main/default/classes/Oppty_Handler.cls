public class Oppty_Handler {
    public static void notificar(List<Opportunity> lstOpptys){
        Set<Id> setOwners = new Set<Id>();
        for(Opportunity objOppty:lstOpptys){
            String owner = objOppty?.OwnerId;
            if(owner.startsWith('005')){
                setOwners.add(owner);
            }
        }
        
        Map<Id, User> mapUsers = new Map<Id,User>([SELECT Id, Email FROM User WHERE Id in: setOwners]);
        List<Messaging.SingleEmailMessage> lstMails = new List<Messaging.SingleEmailMessage>();
        for(Opportunity objOppty:lstOpptys){
            if(mapUsers.containsKey(objOppty.OwnerId)){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new List<String>{mapUsers.get(objOppty.OwnerId).Email});
                mail.setSubject('Opportunity '+objOppty.StageName);
                mail.setHtmlBody('<a href="/'+objOppty.Id+'">Ver Oportunidad<a>');
                lstMails.add(mail);
            }
        }
        try{
        	Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(lstMails);    
        }
        catch(Exception e){
            system.debug('Error enviando notificación');
        }
    }
    
    public static void primeraOportunidadCuenta(List<Opportunity> lstOpptys){
        Set<Id> setAccounts = new Set<Id>();
        Set<Id> setIdsOpptys = new Set<Id>();
        for(Opportunity objOppty:lstOpptys){
            setAccounts.add(objOppty.AccountId);
            setIdsOpptys.add(objOppty.Id);
        }
        
        List<Opportunity> lstAccountsOpptys = [SELECT AccountId
                                               FROM Opportunity
                                               WHERE Id not in :setIdsOpptys
                                               AND AccountId in: setAccounts];
        
        Set<Id> setAccountsExists = new Set<Id>();
        for(Opportunity objAccountsOpptys:lstAccountsOpptys){
            setAccountsExists.add(objAccountsOpptys.AccountId);
        }
        
        List<Account> lstAccountUpdate = new List<Account>();
        for(Opportunity objOppty:lstOpptys){
            if(!setAccountsExists.contains(objOppty.AccountId)){
                Account objCuenta = new Account(Id=objOppty.AccountId, TipoCliente__c = 'Nuevo');
                lstAccountUpdate.add(objCuenta);
            }
        }
        
        if(!lstAccountUpdate.isEmpty()){
            update lstAccountUpdate;
        }
    }
}