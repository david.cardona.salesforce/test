@isTest
public class Oppty_Test {
    @testSetup
    public static void setup(){
        Account objAccount = new Account();
        objAccount.Name = 'test test';
        insert objAccount;
        
        Opportunity objOppty = new Opportunity();
        objOppty.Name = 'test test';
        objOppty.AccountId = objAccount.Id;
        objOppty.StageName = 'Prospecting';
        objOppty.CloseDate = system.today().addDays(30);
        insert objOppty;
            
    }
    
    @isTest
    public static void insertOppty(){
        Account objAccount = [SELECT Id, TipoCliente__c FROM Account Limit 1];
        Opportunity objOppty = new Opportunity();
        objOppty.Name = 'test2 test2';
        objOppty.AccountId = objAccount.Id;
        objOppty.StageName = 'Prospecting';
        objOppty.CloseDate = system.today().addDays(30);
        insert objOppty;
        
        Opportunity opptyNew = [SELECT Id, Account.TipoCliente__c FROM Opportunity WHERE Id = :objOppty.Id];
        
        system.assertEquals(objAccount.TipoCliente__c, 'Nuevo');
    }
    
    @isTest
    public static void updateOppty(){
        Opportunity objOppty = [SELECT Id FROM Opportunity Limit 1];
        objOppty.StageName = 'Closed Lost';
        objOppty.Comentarios__c = 'test';
        update objOppty;
        
        List<EmailMessage> lstEmails = [SELECT Id FROM EmailMessage Limit 1];
        system.assertEquals(lstEmails.isEmpty(), false);
    }
}