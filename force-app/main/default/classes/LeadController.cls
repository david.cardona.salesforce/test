public class LeadController {
	@AuraEnabled
    public static String createLead(Lead objLead){
        List<Lead> objLeadExist = [SELECT Id, Opportunity__c, Opportunity__r.IsClosed, Opportunity__r.AccountId
                                   FROM Lead
                                   WHERE Email = :objLead.Email];
        
        if(!objLeadExist.isEmpty()){
            if(!objLeadExist[0].Opportunity__r.IsClosed){
                Opportunity objOpptyUpdate = new Opportunity(Id=objLeadExist[0].Opportunity__c, StageName = 'Closed Lost', Comentarios__c = 'Actualización prospecto');
                update objOpptyUpdate;
                
                Opportunity objOppty = new Opportunity();
                objOppty.Name = objLead.FirstName + ' ' + objLead.LastName;
                objOppty.AccountId = objLeadExist[0].Opportunity__r.AccountId;
                objOppty.StageName = 'Prospecting';
                objOppty.CloseDate = system.today().addDays(30);
                insert objOppty;
                
                objLead.Opportunity__c = objOppty.Id;
            }
            
            Lead leadUpdate = new Lead();
            leadUpdate.Id = objLeadExist[0].Id;
            leadUpdate.Email = objLead.Email;
            leadUpdate.FirstName = objLead.FirstName;
            leadUpdate.LastName = objLead.LastName;
            leadUpdate.Company = objLead.Company;
            leadUpdate.Opportunity__c = objLead.Opportunity__c;
            update leadUpdate;
            
           	return 'updated';
        }
        else{
            Account objAccount = new Account();
            objAccount.Name = objLead.FirstName + ' ' + objLead.LastName;
            insert objAccount;
            
            Opportunity objOppty = new Opportunity();
            objOppty.Name = objLead.FirstName + ' ' + objLead.LastName;
            objOppty.AccountId = objAccount.Id;
            objOppty.StageName = 'Prospecting';
            objOppty.CloseDate = system.today().addDays(30);
            insert objOppty;
            
            objLead.Opportunity__c = objOppty.Id;
            insert objLead;
            
            enviarNotificacion(objLead.Id);
            return 'created';
        }
        
    }
    
    public static void enviarNotificacion(Id idLead){
        try{
            Lead objLead = [SELECT Id, Name, ownerId, owner.Email
                            FROM Lead
                            WHERE Id = :idLead];
            
            String owner = objLead?.OwnerId;
            
            if(owner.startsWith('005')){
                List<Messaging.SingleEmailMessage> lstMails = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new List<String>{objLead.owner.Email});
                mail.setSubject('Nuevo prospecto:'+objLead.Name);
                mail.setHtmlBody('<a href="/'+idLead+'">Ver Prospecto<a>');
                lstMails.add(mail);
                Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(lstMails);
            }
        }
        catch(Exception e){
            system.debug('Error enviando notificación');
        }
    }
}