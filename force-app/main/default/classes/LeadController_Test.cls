@isTest
public class LeadController_Test {
    @isTest
    public static void createLeadNew(){
        Lead objLead = new Lead();
        objLead.Email = 'test@test.com';
        objLead.FirstName = 'test';
        objLead.LastName = 'test';
        objLead.Company = 'test';
        LeadController.createLead(objLead);
        
        Lead objLeadInsert = [SELECT Id FROM Lead];
        Opportunity objOpptyInsert = [SELECT Id FROM Opportunity];
        Account objAccountInsert = [SELECT Id FROM Account];
        system.assertNotEquals(objLeadInsert, null);
        system.assertNotEquals(objOpptyInsert, null);
        system.assertNotEquals(objAccountInsert, null);
    }
    
    @isTest
    public static void createLeadExist(){
        Lead objLead = new Lead();
        objLead.Email = 'test@test.com';
        objLead.FirstName = 'test';
        objLead.LastName = 'test';
        objLead.Company = 'test';
        LeadController.createLead(objLead);
        
        Lead objLead2 = new Lead();
        objLead2.Email = 'test@test.com';
        objLead2.FirstName = 'test 2';
        objLead2.LastName = 'test 2';
        objLead2.Company = 'test 2';
        LeadController.createLead(objLead2);
        
        Opportunity objOppty = [SELECT Id FROM Opportunity WHERE StageName = 'Closed Lost'];
        system.assertNotEquals(objOppty, null);
    }
}